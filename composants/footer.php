<footer class="footer bg-success">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<a href=""><img src="./Images/pprofil.png"></a>
			</div>
			<div class="col-md-4">
				<ul>
					<li>Customer Support Centre</li>
					<li>+33 1 23 45 67 89</li>
					<li><a href="http://www.centres-antipoison.net/">Centre Anti-poison</a></li>
				</ul>
			</div>
			<div class="col-md-4">
				<ul>
					<li>Qui sommes nous ?</li>
					<li>Où nous trouver ?</li>
					<li>Pourquoi cette idée ?</li>
				</ul>
			</div>
		</div>
		<div class="row border-top mt-3">
			<p class="mx-auto pt-2">Site réalisé par Sarah Brood, Samuel Michel, Arthur Vadrot et Bootstrap</p>
		</div>
	</div>
</footer>